# Lovely Weddings

Simple HTML,Css & Javascript based Wedding Blog.

## Features

- Slider
- Customized Header
- Date Countdown
- Love Story Timeline
- Events
- Groomsmen & Bridesmaid Section
- RSVP Form
- Gallery

## Hosted
[Live URL](https://lovely-weddings-pritamdhara-5f96746da4ca9131eccb9a0ffbb8df3b1ac.gitlab.io/)
